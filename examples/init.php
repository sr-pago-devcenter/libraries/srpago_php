<?php

$srpagoFunctions = array(
  'json_decode'=>'SrPago requires JSON extension',
  'curl_init'=>'SrPago requires CURL extension',
  'mb_detect_encoding'=>'SrPago requires MB (Multibyte String) extension',
  'openssl_public_encrypt'=>'SrPago requires openssl extension',
);

if(version_compare(PHP_VERSION, '5.3.3', '<')) {
  throw new Exception('SrPago should be run on PHP >= 5.3.3');
}

foreach($srpagoFunctions as $method=>$message){
  if (!function_exists($method)) {
      throw new Exception($message);
    }
}
if (!version_compare(PHP_VERSION, '7.1.0', '>=')) {
  if (!function_exists('mcrypt_encrypt')) {
    throw new Exception('SrPago requires mcrypt extension');
  }
}


// SrPago singleton
require( '../lib/SrPago.php');

// Errors
require( '../lib/Error/SrPagoError.php');

//HttpClient
require( '../lib/Http/HttpClient.php');

require( '../lib/Util/Encryption.php');

// Resources
require( '../lib/Base.php');

// SrPago API Resources
require( '../lib/Token.php');
require( '../lib/Operations.php');
require( '../lib/Charges.php');
require( '../lib/Customer.php');
require( '../lib/CustomerCards.php');
