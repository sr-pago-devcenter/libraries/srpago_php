<?php
/**
 * Server-side tokenization example
 */

// Import required libraries
  include_once './SrPago.php';
  include_once ('header.php');

// 1. Tokenize a new card

    $response = '';

    $card = array(
        "number"=>'4929464181360031',
        "holder_name" => 'Juan Doe',
        "cvv"=> '123',
        "expiration"=> '2311'
        );

    try {
        $srPago = new SrPago();
        $srPago->setup();
        $response =  $srPago->createCardToken($card);
    
    }catch (Exception $e){
      echo 'Error ' . $e->getMessage() . ' ' . $e->getFile();
    }

    $token = $response['result']['token'];
    echo "<h2>1. Card Token</h2>";
    echo "<h3>Request body - data without encoding</h3>";
    print_r($card);
    echo "<h3>Response - token obtained</h3>";
    print_r($token);
    

// 2. Create a charge using token obtained in step 1.

    $chargeParams = array(
        "amount"=>10,
        "description" => 'Test',
        "reference"=> 'test',
        "ip"=> getIp(),
        "source"=> $token,
        );

    // example metadata (required - for more information check the devcenter)

        $metadata = array(
            "items"=>array(
               "item" => array(
                   array(
                     "itemNumber"=> "193487654",
                     "itemDescription"=> "iPhone 6 32gb",
                     "itemPrice"=> "599.00",
                     "itemQuantity"=> "1",
                     "itemMeasurementUnit"=> "Pza",
                     "itemBrandName"=> "Apple",
                     "itemCategory"=> "Electronics",
                     "itemTax"=> "12.95"
                 ),
               )
             )
          );

    try {
        $srPago = new SrPago();
        $srPago->setup();
        $response =  $srPago->chargesCreateCharge($chargeParams, $metadata);

    }catch (Exception $e){
        echo 'Error ' . $e->getMessage() . ' ' . $e->getFile();
    }
    
    echo "<h2>Create Charge</h2>";

    echo "<h3>Charge parameters without encoding</h3>";
    print_r($chargeParams);

    echo "<h3>Response</h3>";
    print_r($response);

    function getIp(){
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if(isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if(isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }