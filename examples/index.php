<?php
/**
 * Example: Single use card tokenization
 */
?>

<!doctype html>
<html>
<head>
    <title>Create a single use Card Token</title>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <script type="text/javascript" src="https://js.srpago.com/v1/srpago.min.js"></script>
    <script type="text/javascript" src="https://js.srpago.com/v1/srpago.encryption.min.js"></script>
    <script type="text/javascript">


        $( document ).ready(function() {
            SrPago.setLiveMode(false); // false = Sandbox true = Production
            SrPago.setPublishableKey(""); // Your App Public Key

            $('#card-payment-form-button').click(function(event) {
                 console.log('ENTRA');

                var onSuccessHandler = function(result){
                    $("#response_message").html("With this token you can process a charge on your server:  " + result.token);
                    $("#tokenInput").val(result.token);
                    $("#card-payment-form").submit();
                };

                var onFailHandler = function(error){
                    $("#response_message").html("Error " + error.code + ": " + error.message);
                };

                var card = {
                    number: $('.card-number').val(),
                    holder_name: $('.card-holder-name').val(),
                    cvv: $('.card-cvv').val(),
                    exp_month: $('.card-exp-month').val(),
                    exp_year: $('.card-exp-year').val()
                };

                SrPago.token.create(card,onSuccessHandler,onFailHandler);


                return false;

            });

        });

    </script>
</head>
<?php include_once ('header.php'); ?>


<body>
<h1>Create a $10 MXN charge </h1>

<dl>
    <dt>Card Number</dt>
    <dd><input type="text" name="number" maxlength="16"  value=""  class="card-number"  data-card="number" placeholder="Número de tarjeta"/></dd>
    <dt>Cardholder Name</dt>
    <dd><input type="text" name="holder_name" value=""  class="card-holder-name"  data-card="holder_name"  placeholder="Tarjetahabiente"/></dd>
    <dt>Expiration Date MM/YY</dt>
    <dd>
        <input type="text" name="month" maxlength="2" size="2" class="card-exp-month"  data-card="exp_month" value=""/>
        <input type="text" name="year" maxlength="2" size="2"  class="card-exp-year"  data-card="exp_year" value=""/>
    </dd>
    <dt>CVV</dt>
    <dd>
        <input type="password" name="cvv" size="4" data-card="cvv" class="card-cvv"  value=""/>
    </dd>
</dl>
<form action="payment.php" method="POST" id="card-payment-form">
    <input type="hidden" name="tokenInput" id="tokenInput" />
    <input type="button" id="card-payment-form-button" value="Pay Now"/>
    <hr/><label id="response_message"></label>
</form>

</body>
</html>
