<?php
/**
 * Example: Customer admininistration plus add card token to customer
 */


include_once './SrPago.php';
include_once ('header.php');

$srPago = new SrPago();
$srPago->setup();


$data['email'] = $_POST['email'];
$data['name'] = $_POST['name'];

$customer = [];


    if(!isset($_GET['clientToken'])){
        try {
            $customer = $srPago->createCustomer($data);
        }catch (Exception $e){
            echo 'Error ' . $e->getMessage() . ' ' . $e->getFile();
        }

        $clientToken = $customer['result']['id'];
    }else{
        $clientToken = $_GET['clientToken'];
    }

    try {
        $customer = $srPago->findCustomer($clientToken);
    }catch (Exception $e){
        echo 'Error ' . $e->getMessage() . ' ' . $e->getFile();
    }

?><h1>Result of Customer request:</h1>
<?php
print_r($customer);
    ?>
<head>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <script type="text/javascript" src="https://js.srpago.com/v1/srpago.min.js"></script>
    <script type="text/javascript" src="https://js.srpago.com/v1/srpago.encryption.min.js"></script>
    <script type="text/javascript">


    $( document ).ready(function() {
        SrPago.setLiveMode(false); // false = Sandbox true = Producción

        SrPago.setPublishableKey(" "); // Aquí va tu Plublic Key


        $('#card-payment-form-button').click(function(event) {

            var onSuccessHandler = function(result){
                $("#response_message").html("Con este token procesará el pago desde su Servidor " + result.token);
                $("#tokenInput").val(result.token);
                $("#card-payment-form").submit();
            };

            var onFailHandler = function(error){
                $("#response_message").html("Error " + error.code + ": " + error.message);
            };

            var card = {
                number: $('.card-number').val(),
                    holder_name: $('.card-holder-name').val(),
                    cvv: $('.card-cvv').val(),
                    exp_month: $('.card-exp-month').val(),
                    exp_year: $('.card-exp-year').val()
                };

                SrPago.token.create(card,onSuccessHandler,onFailHandler);


                return false;

            });

    });

    </script>


</head>

<h1>Add card to Customer</h1>
<dl>
    <dt>Card Number</dt>
    <dd><input type="text" name="number" maxlength="16"  value=""  class="card-number"  data-card="number" placeholder="Número de tarjeta"/></dd>
    <dt>Cardholder Name</dt>
    <dd><input type="text" name="holder_name" value=""  class="card-holder-name"  data-card="holder_name"  placeholder="Tarjetahabiente"/></dd>
    <dt>Expiration Date MM/YY</dt>
    <dd>
        <input type="text" name="month" maxlength="2" size="2" class="card-exp-month"  data-card="exp_month" value=""/>
        <input type="text" name="year" maxlength="2" size="2"  class="card-exp-year"  data-card="exp_year" value=""/>
    </dd>
    <dt>CVV</dt>
    <dd>
        <input type="password" name="cvv" size="4" data-card="cvv" class="card-cvv"  value=""/>
    </dd>
</dl>
<form action="addCard.php" method="POST" id="card-payment-form">
    <input type="hidden" name="tokenInput" id="tokenInput" />
    <input type="hidden" name="clientToken" id="tokenInput" value="<?php echo $clientToken  ?>" />
    <input type="button" id="card-payment-form-button" value="Add Card"/>
    <hr/><label id="response_message"></label>
</form>
