<?php
/**
 * Example: Create charge with Token
 */

// Required libraries
  include_once './SrPago.php';
  include_once ('header.php');


// Charge parameters

$chargeParams = array(
    "amount"=>10,
    "description" => 'Test',
    "reference"=> 'test',
    "ip"=> getIp(),
    "source"=>$_POST['tokenInput'],  // Card token
    // "user" => "locanto@srpago.com", // Uncomment if you are using JOIN
    //"total_fee" => "3"// Uncomment if you are using JOIN
   // )
    );
    
//$paymentRQ = array(
    //"user" => "locanto@srpago.com",
   // "total_fee" => "3"
  //  );

$metadata = array(    );

$response = '';
try {
    $srPago = new SrPago();
    $srPago->setup();
    $response =  $srPago->chargesCreateCharge($chargeParams, $metadata);


}catch (Exception $e){
  echo 'Error ' . $e->getMessage() . ' ' . $e->getFile();

}

print_r($response);

function getIp(){
    $ipaddress = '';
    if (isset($_SERVER['HTTP_CLIENT_IP']))
        $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
    else if(isset($_SERVER['HTTP_X_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_X_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
    else if(isset($_SERVER['HTTP_FORWARDED_FOR']))
        $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
    else if(isset($_SERVER['HTTP_FORWARDED']))
        $ipaddress = $_SERVER['HTTP_FORWARDED'];
    else if(isset($_SERVER['REMOTE_ADDR']))
        $ipaddress = $_SERVER['REMOTE_ADDR'];
    else
        $ipaddress = 'UNKNOWN';
    return $ipaddress;
}