<?php

/**
 * Example implementations
 */

include_once 'init.php';

class SrPago
{

    public function Setup(){

        \SrPago\SrPago::$apiKey = ""; // Here add the App Key
        \SrPago\SrPago::$apiSecret = ""; // App Secret Key
        \SrPago\SrPago::$liveMode = false; // false = Sandbox - true = Production

    }


    /*
    *
    *    Create a new customer - required for on demand tokenization
    *
    */
    /**
     *
     * @param array $parameters
     * @return mixed
     */
    public function createCustomer($data){
        $customerService = new \SrPago\Customer();

        $newCustomer = $customerService->create($data);

        return $newCustomer;

    }

    /*
     *
     * Function to find a specific customer
     *
     */
    /**
     *
     * @param array $parameters
     * @return mixed
     */
    public function findCustomer($data){
        $customerService = new \SrPago\Customer();

        $newCustomer = $customerService->find($data);

        return $newCustomer;
    }


    /*
     *
     * Add a card to a customer (On demand tokenization)
     *
     */

    /**
     *
     * @param array $parameters
     * @return mixed
     */
    public function addCardToCustomer($user, $token){
        $customerCardService = new \SrPago\CustomerCards();

        $newCard = $customerCardService->add($user, $token);

        return $newCard;
    }


    /*
     *
     * Remove a card from a customer
     *
     */

    /**
     *
     * @param string $user
     * @param string token
     * @return mixed
     */
    public function removeCustomerCard($user, $token){
        $customerCardService = new \SrPago\CustomerCards();

        $removedCard = $customerCardService->remove($user, $token);

        return $removedCard;
    }


    /*
     *
     * Create a charge
     *
     */

    /**
     *
     * @param array $chargeParams
     * @param array metadata
     * @return mixed
     */
    public function ChargesCreateCharge($chargeParams, $metadata){
        $chargesService = new \SrPago\Charges();

        $chargeParams['metadata'] = $metadata;

        $newCharge = $chargesService->create($chargeParams);

        return $newCharge;
    }

    /*
     *
     * Server side tokenization
     *
     */

    /**
     *
     * @param array $chargeParams
     * @param array metadata
     * @return mixed
     */
    public function createCardToken($card){
        $tokenService = new \SrPago\Token();

        $token = $tokenService->create($card);

        return $token;
    }

}

?>